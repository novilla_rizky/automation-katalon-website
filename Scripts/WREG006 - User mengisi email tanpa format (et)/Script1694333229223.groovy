import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('WREG/Page_Be a Profressional Talent with Coding.ID/button_Buat                                                        Akun'))

WebUI.setText(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), 'sumanti')

WebUI.setText(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), '1-sep-2007')

WebUI.sendKeys(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), Keys.chord(
		Keys.ENTER))

WebUI.setText(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'), 'sumantiyahoo.com')

WebUI.setText(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), '081234567890')

WebUI.setEncryptedText(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Kata Sandi_password'),
	'fW9ZQjjT/iTl/4gte9umXA==')

WebUI.setEncryptedText(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_password_confirmation'),
	'fW9ZQjjT/iTl/4gte9umXA==')

WebUI.click(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.click(findTestObject('WREG/Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/WREG/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'),
		'validationMessage'), 'Please include an \'@\' in the email address. \'sumantiyahoo.com\' is missing an \'@\'.')

WebUI.takeScreenshot()

WebUI.closeBrowser()

